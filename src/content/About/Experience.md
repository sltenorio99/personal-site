---
templateKey: about-section
title: Current Status
listOrder: '3'
---

### Cerner

#### Software Engineer Intern

- Developer of multiple node web applications that span across internal and client uses

  - React.js front-end
  - Express.js

- Maintain team UI component library
- Create and maintain stored procedures to get application data
