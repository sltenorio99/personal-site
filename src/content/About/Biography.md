---
templateKey: about-section
title: Biography
listOrder: '2'
---

### Intro

Hello there! My name is Shane-Lewes Tenorio. I am an Associate Senior Software Engineer with a Bachelor's Degree in Computer Science. I'm curious about technology and I like to learn new things. If I'm not near a computer; I'm either at the gym, playing soccer, or relaxing.
