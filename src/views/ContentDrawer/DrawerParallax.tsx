import React from 'react';
import { Parallax, ParallaxLayer } from 'react-spring/dist/addons';

// Export default instance of Parallax component
export default Parallax;

// To work, set DrawerParallaxLetter with `position: relative`
// and inner div of Parallax with `height: auto`

// Create extended class from ParallaxLayer
export class DrawerParallaxLayer extends ParallaxLayer {
  // Override ParallaxLayer setPosition to allow
  // setting limits to translation
  setPosition(height: number, scrollTop: number, immediate: boolean = false) {
    const { offset, speed } = this.props;
    const { config, impl } = this.parent.props;
    const targetScroll = Math.floor(offset) * height;
    const offsetCalc = height * offset + targetScroll * speed;
    const to = parseFloat(-(scrollTop * speed) + offsetCalc);
    if (to >= 886) super.setPosition(height, scrollTop, immediate);
  }

  // Override ParallaxLayer setHeight to allow
  // setting height from props
  setHeight(height: number, immediate: boolean = false) {
    const { isOverDrawer } = this.props;
    const { config, impl } = this.parent.props;
    console.log('Height:', height, 'isOverDrawer', isOverDrawer);
    if (isOverDrawer) {
      controller(this.animatedSpace, { to: 'auto', ...config }, impl).start();
    } else super.setHeight(height, immediate);
  }
}
