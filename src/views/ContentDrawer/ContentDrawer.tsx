import React from 'react';
import SheetDrawer from '../../components/sheet-drawer';

export default class ContentDrawer extends React.PureComponent {
  render() {
    const { children } = this.props;
    return <SheetDrawer {...this.props}>{children}</SheetDrawer>;
  }
}
