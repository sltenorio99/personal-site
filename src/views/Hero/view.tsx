import React from "react";
import { Wrapper } from "../../components/hero";
import { Box, Text, Heading } from "rebass";
import ProfileImage from "./styled/ProfileImage";
import PersonalPicture from "../../resources/images/Personal-Picture.jpg";

export default class View extends React.Component {
  render() {
    return (
      <Wrapper alignItems="center" flexDirection="column">
        <Box>
          <ProfileImage
            src={PersonalPicture}
            height={[100, 100, 100, 150]}
            width={[100, 100, 100, 150]}
          />
        </Box>
        <Heading fontSize={[4, 4, 4, 5]} my={[2, 3]}>
          Shane-Lewes Tenorio
        </Heading>
        <Text fontSize={[1, 1, 1, 2]}>Software Developer</Text>
      </Wrapper>
    );
  }
}
