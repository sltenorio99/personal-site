import styled from "styled-components";
import { Image } from "rebass";

const ProfilePicture = styled(Image)`
  border-radius: 100%;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
`;

export default ProfilePicture;
