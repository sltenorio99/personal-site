import React from 'react';
import { Layer, Drawer } from '../../components/parallax-drawer';
import Card from '../../components/card';
import { Flex, Text, Heading } from 'rebass';
import Lorem from 'react-lorem-component';
import { graphql } from 'gatsby';

const AboutPage = ({ content, onClick }) => {
  return (
    <Flex flexDirection='column' mt={[2, 5]}>
      <Layer>
        <Heading
          fontSize={[4, 5]}
          fontWeight={500}
          letterSpacing={'0.016em'}
          mt={5}
          mb={4}
          color='white'
        >
          About Me
        </Heading>
      </Layer>
      {content.map(({ node }, idx) => {
        const { html, frontmatter } = node;
        return (
          <Layer key={'layer_' + frontmatter.title + idx}>
            <Drawer key={frontmatter.title + idx} onClick={onClick}>
              <div>
                <Heading fontSize={[3, 4]} color='#0699ff'>
                  {frontmatter.title}
                </Heading>
                <div dangerouslySetInnerHTML={{ __html: html }} />
              </div>
            </Drawer>
          </Layer>
        );
      })}
    </Flex>
  );
};

export default AboutPage;
