import React from 'react';
import { Layer, Drawer, DrawerList } from '../../components/parallax-drawer';
import {
  DrawerContent,
  ItemDescription,
  ItemMeta
} from '../../components/parallax-drawer/Drawer';
import Card from '../../components/card';
import { Flex, Link, Heading } from 'rebass';
import Lorem from 'react-lorem-component';
import ky from 'ky';

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      githubRepos: [],
      gitlabProjects: []
    };
  }

  componentDidMount() {
    this.getGitlabProjects();
  }

  getGitlabProjects = async () => {
    const gitlabPublicProjects = await ky
      .get(`https://gitlab.com/api/v4/users/sltenorio99/projects`, {
        searchParams:
          `private_token=${process.env.GITLAB_TOKEN}&` +
          `visibility=public&` +
          `order_by=last_activity_at`
      })
      .json();

    const gitlabInternalProjects = await ky
      .get(`https://gitlab.com/api/v4/users/sltenorio99/projects`, {
        searchParams:
          `private_token=${process.env.GITLAB_TOKEN}&` +
          `visibility=internal&` +
          `order_by=last_activity_at`
      })
      .json();

    const gitlabProjects = [
      ...gitlabPublicProjects,
      ...gitlabInternalProjects
    ].sort((a, b) => {
      if (a.last_activity_at > b.last_activity_at) return -1;
      if (a.last_activity_at < b.last_activity_at) return 1;
      return 0;
    });

    this.setState({ gitlabProjects });
  };

  render() {
    const { githubRepos, gitlabProjects } = this.state;
    const { content } = this.props;

    return (
      <Flex flexDirection='column'>
        <Layer>
          <Heading
            fontSize={[4, 5]}
            fontWeight={500}
            letterSpacing={'0.016em'}
            mt={5}
            mb={4}
            color='white'
          >
            Projects
          </Heading>
        </Layer>
        <Layer>
          <Drawer>
            <Heading fontSize={[3, 4]} color='#0699ff'>
              Gitlab Projects
            </Heading>
            <DrawerContent>
              <DrawerList>
                {gitlabProjects.map((project, idx) => (
                  <li key={project.name + idx}>
                    <Flex>
                      <Heading fontSize={2}>{project.name}</Heading>
                      <Link href={project.web_url} fontSize={1} ml='auto'>
                        Repo Link
                      </Link>
                    </Flex>
                    <ItemDescription>
                      {project.description ||
                        'No description was made for this project...'}
                    </ItemDescription>
                    <ItemMeta>
                      <div
                        style={{
                          background: 'gray',
                          color: 'white',
                          fontSize: '0.8rem',
                          padding: '2px 8px',
                          borderRadius: 4
                        }}
                      >
                        {project.visibility}
                      </div>
                    </ItemMeta>
                  </li>
                ))}
              </DrawerList>
            </DrawerContent>
          </Drawer>
        </Layer>
        <Layer>
          <Drawer onClick={this.props.onClick}>
            <Heading fontSize={[3, 4]} color='#0699ff'>
              Github Repos
            </Heading>
            <DrawerContent>
              <DrawerList>
                {content.ownedRepos.map((repo, idx) => (
                  <li key={repo.name + idx}>
                    <Flex>
                      <Heading fontSize={2}>{repo.name}</Heading>
                      <Link href={repo.url} fontSize={1} ml='auto'>
                        Repo Link
                      </Link>
                    </Flex>
                    <ItemDescription>
                      {repo.description ||
                        'No description was made for this project...'}
                    </ItemDescription>
                    <ItemMeta>
                      {repo.isFork && (
                        <div
                          style={{
                            background: 'purple',
                            color: 'white',
                            fontSize: '0.8rem',
                            padding: '2px 8px',
                            borderRadius: 4
                          }}
                        >
                          Fork
                        </div>
                      )}
                    </ItemMeta>
                  </li>
                ))}
              </DrawerList>
            </DrawerContent>
          </Drawer>
        </Layer>
        <Layer>
          <Drawer onClick={this.props.onClick}>
            <Heading fontSize={[3, 4]} color='#0699ff'>
              Contributed Repos
            </Heading>
            <DrawerContent>
              <DrawerList>
                {content.contributedRepos.map((repo, idx) => (
                  <li key={repo.name + idx}>
                    <Flex>
                      <Heading fontSize={2}>{repo.name}</Heading>
                      <Link href={repo.url} fontSize={1} ml='auto'>
                        Repo Link
                      </Link>
                    </Flex>
                    <ItemDescription>
                      {repo.description ||
                        'No description was made for this project...'}
                    </ItemDescription>
                    <ItemMeta />
                  </li>
                ))}
              </DrawerList>
            </DrawerContent>
          </Drawer>
        </Layer>
      </Flex>
    );
  }
}
