import React from "react";
import { Flex, Box, Text } from "rebass";
import Header from "../../components/header";

export default props => (
  <Header ref={props.headerRef}>
    <Flex alignItems="flex-end">
      <Box>
        <Box style={{ lineHeight: "32px", marginLeft: "auto" }}>
          Placeholder
        </Box>
        <h1 style={{ marginBottom: 0, marginTop: 12 }}>Shane-Lewes Tenorio</h1>
        <Text>Software Developer</Text>
      </Box>
    </Flex>
  </Header>
);
