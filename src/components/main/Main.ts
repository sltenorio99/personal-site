import styled from "styled-components";

const Main = styled.main`
  position: relative;
  z-index: 10;
`;

export default Main;
