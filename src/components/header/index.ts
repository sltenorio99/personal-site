import React from 'react';
import styled from 'styled-components';
import Section from '../section';

const Header = styled(Section)`
  width: 100%;
  background: transparent;
  color: #fff;
  position: relative;
  left: 0;
  top: 0;
  padding-bottom: 16px;
  pointer-events: none;
  position: absolute;
  z-index: 10;
  height: auto;
`;

export default Header;
