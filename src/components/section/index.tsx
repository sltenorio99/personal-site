import React from "react";
import styled from "styled-components";
import { Box } from "rebass";

const StyledBox = styled(Box)`
  width: 100%;
  position: relative;
  left: 0;
  top: 0;
  max-height: 50%;
  box-sizing: border-box;
  margin: 20px 0;
  border-radius: 20px;
  overflow: hidden;
  background: rgba(240, 240, 240, 0.9);
  &:first-child {
    margin-top: 25vh;
  }
`;

const Section = props => {
  // console.log(props);
  return (
    <StyledBox py={4} px={4} {...props}>
      {props.children}
    </StyledBox>
  );
};

export default Section;
