import react from 'react';
import styled from 'styled-components';
import { Card } from 'rebass';

const StyledCard = styled(Card)`
  border-radius: 5px;
  display: inline-block;
  box-sizing: border-box;
`;

export default StyledCard;
