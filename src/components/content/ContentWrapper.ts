import styled from "styled-components";
import { Flex } from "rebass";

const ContentWrapper = styled(Flex)`
  margin-left: auto;
  margin-right: auto;
  @media screen and (min-width: 40em) {
    padding-left: 64px;
    padding-right: 64px;
  }
  @media screen and (min-width: 70em) and (max-width: 80em) {
    padding-left: 128px;
    padding-right: 128px;
  }
`;

export default ContentWrapper;
