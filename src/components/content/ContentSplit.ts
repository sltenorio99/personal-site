import styled from "styled-components";
import { Box } from "rebass";

const ContentSplit = styled(Box)`
  align-items: center;
  position: relative;
  max-width: 650px;
`;

export default ContentSplit;
