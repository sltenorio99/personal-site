import React from "react";
import styled from "styled-components";
import { Flex, Text } from "rebass";

const borderRadius = 20;

const StyledBox = styled(Flex)`
  width: 100%;
  position: relative;
  flex-direction: column;
  left: 0;
  top: 0;
  min-height: 50%;
  max-height: 78vh;
  box-sizing: border-box;
  margin: 20px 0;
  border-radius: ${borderRadius}px;
  overflow: hidden;
  background: rgba(255, 255, 255, 0.9);
  backdrop-filter: blur(20px) saturate(6);
  & > div {
    position: relative;
    z-index: 2;
  }
`;

export const DrawerContent = styled(Flex)`
  height: calc(100% + 32px);
  width: calc(100% + 64px);
  left: -32px;
  margin-top: 1.5rem;
  margin-bottom: -32px;
  top: 0;
  flex: 1 1 auto;
  overflow: hidden;
`;

export const DrawerList = styled("ul")`
  position: relative;
  width: 100%;
  margin: 0;
  padding: 0;
  list-style-type: none;
  border-top: 1px solid #d1d1d1;
  box-sizing: border-box;
  flex: 1 1 auto;
  overflow: auto;
  overflow-x: hidden;
  li {
    position: relative;
    height: 100px;
    margin: 0;
    padding: 1rem 32px;
    border-bottom: 1px solid #d1d1d1;
    .list-item-header {
      margin: 0 0 0.1em 0;
      font-size: 16px;
    }
    &:last-child {
      margin-bottom: 12px;
    }
    :hover {
      background: rgba(0, 0, 0, 0.07);
    }
  }
`;

export const ItemDescription = styled(Text)`
  font-size: 0.9rem;
  line-height: 1.1rem;
  display: block;
  max-height: 2.25rem;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

export const ItemMeta = styled(Flex)`
  width: 100%;
  font-size: 0.9rem;
  line-height: 1.1rem;
  position: absolute;
  bottom: 8px;
`;

const Drawer = props => {
  // console.log(props);
  return (
    <StyledBox py={4} px={4} {...props}>
      {props.children}
    </StyledBox>
  );
};

export default Drawer;
