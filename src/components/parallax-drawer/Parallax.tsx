import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import { Controller, config, Globals, animated } from 'react-spring';
import { parsePath } from 'history';
import { element } from 'prop-types';

const El = Globals.defaultElement;
const AnimatedDiv = animated(El);
const { Provider, Consumer } = React.createContext(null);

export { Consumer };

export default class Parallax extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { ready: false };
    this.layers = [];
    this.activeLayer = null;
    this.controller = new Controller({ scroll: 0 });
    this.container = null;
  }

  componentDidMount() {
    const { target } = this.props;
    if (target) {
      target.addEventListener('scroll', this.onScroll);
    }
  }

  addLayer = layer => {
    this.layers = this.layers.concat(layer);
    this.checkActiveLayer(layer, this.layers.length - 1);
  };

  onScroll = e => {
    const scrollElement = e.target.scrollingElement
      ? e.target.scrollingElement
      : e.target;
    this.layers.forEach((layer, idx) => {
      this.checkActiveLayer(layer, idx);
    });
  };

  checkActiveLayer = (layer, index) => {
    const layerEl = ReactDOM.findDOMNode(layer);
    const rect = layerEl.getBoundingClientRect();
    const clientHeight = document.documentElement.clientHeight;
    if (rect.top > 50 && rect.top < 300) {
      layer.setActive();
    }
    // else if (rect.bottom < 300 || rect.bottom - clientHeight > 0) {
    //   layer.setInactive(index);
    // }
    else {
      layer.setInactive(index);
    }
  };

  render() {
    const { children, target } = this.props;
    return (
      <El
        ref={node => (this.container = node)}
        onScroll={!target ? this.onScroll : undefined}
      >
        <El>
          <Provider value={this}>{children}</Provider>
        </El>
      </El>
    );
  }
}
