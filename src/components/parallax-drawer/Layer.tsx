import React from 'react';
import styled from 'styled-components';
import {
  Controller,
  config,
  Globals,
  animated,
  interpolate
} from 'react-spring';
import { Box } from 'rebass';
import { render } from 'react-dom';
import { Consumer } from './Parallax.tsx';

const El = Globals.defaultElement;
const AnimatedDiv = animated(El);

export default class Layer extends React.PureComponent {
  componentDidMount() {
    const parent = this.parent;
    if (parent) {
      parent.addLayer(this);
    }
  }

  setActive = () => {
    const { activePersist } = this.props;
    if (!this.isActive) {
      // console.log('setting layer active:', this);
      this.controller.update({
        translate: 0,
        scale: 1,
        opacity: 1,
        pointerEvents: 'all',
        zIndex: 100
      });
      this.isActive = true;
    }
  };

  setInactive = index => {
    if (this.isActive || this.isActive == null) {
      // console.log('setting layer inactive:', this);
      this.controller.update({
        translate: 0, // TODO: Decide whether to translate cards
        scale: 0.9,
        opacity: 0.5,
        pointerEvents: 'none',
        zIndex: 1
      });
      this.isActive = false;
    }
  };

  initialize = () => {
    this.controller = new Controller({
      translate: 0, // TODO: Decide whether to translate cards
      scale: 0.9,
      opacity: 0.5,
      pointerEvents: 'none',
      zIndex: 1
    });
  };

  renderLayer = () => {
    const { children } = this.props;
    const { scale, translate } = this.controller.interpolations;
    const transform = interpolate(
      [scale, translate],
      (s, t) => `translate3d(0, ${t}px, 0) scale(${s})`
    );

    return (
      <AnimatedDiv
        {...this.props}
        style={{
          transform,
          position: 'relative',
          willChange: 'transform',
          opacity: this.controller.interpolations.opacity,
          pointerEvents: this.controller.interpolations.pointerEvents
        }}
      >
        {children}
      </AnimatedDiv>
    );
  };

  render() {
    return (
      <Consumer>
        {parent => {
          if (parent && !this.parent) {
            this.parent = parent;
            this.initialize();
          }
          return this.renderLayer();
        }}
      </Consumer>
    );
  }
}
