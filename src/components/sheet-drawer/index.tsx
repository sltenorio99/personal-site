import React from 'react';
import styled from 'styled-components';
import { Box } from 'rebass';

const SheetDrawer = styled(Box)`
  background: #fff;
  border-radius: 20px;
  box-sizing: border-box;
  min-height: 50px;
  height: 100%;
  width: 100%;
  overflow: auto;
`;

export default SheetDrawer;
