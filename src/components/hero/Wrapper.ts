import styled from 'styled-components';
import { Flex } from 'rebass';

const Wrapper = styled(Flex)`
  // TODO: Check for compat later
  position: sticky;
  top: 25%;
  color: rgba(255, 255, 255, 0.9);
  pointer-events: none;
`;

export default Wrapper;
