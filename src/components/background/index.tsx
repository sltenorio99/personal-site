import styled from 'styled-components';
import fuchsiaBackground from '../../resources/images/Fuchsia-Background.jpg';

const Background = styled('div')`
  position: fixed;
  height: 100vh;
  width: 100vw;
  background-color: #21d4fd;
  background-image: url('${fuchsiaBackground}');
  background-size: cover;
  z-index: -1;
  pointer-events: none;
`;

export const BackgroundOverlay = styled('div')`
  position: fixed;
  height: 100vh;
  width: 100vw;
  background-color: #333;
  opacity: 0.8;
  top: 0;
  left: 0;
  z-index: 0;
  pointer-events: none;
`;

export default Background;
