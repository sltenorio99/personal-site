import React from 'react';
import ReactDOM from 'react-dom';
import { createGlobalStyle } from 'styled-components';
import { Flex, Box } from 'rebass';
import Main from '../components/main';
import { ContentWrapper, ContentSplit } from '../components/content';
import Background, { BackgroundOverlay } from '../components/background';
import Hero from '../views/Hero';
import { Parallax } from '../components/parallax-drawer';
import ContentDrawer, { DrawerParallaxLayer } from '../views/ContentDrawer';
import IntroSection from '../sections/Intro';
import AboutSection from '../sections/About';
import ProjectsSection from '../sections/Projects';
import { graphql } from 'gatsby';
// import { Parallax, ParallaxLayer } from 'react-spring';
const GlobalStyle = createGlobalStyle`
body {
  margin: 0;
}
`;

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOverDrawer: false
    };

    this.parallax = React.createRef();
    this.contentRef = React.createRef();
    this.headerRef = React.createRef();
    // this.preventBodyScroll = false;
  }

  onWrapperScroll = e => {
    const { target } = e;
    // If parallax element is scrolled to the bottom,
    // prevent parallax scroll and scroll the content now
    if (target.scrollTop === target.clientHeight) {
      if (!this.preventBodyScroll) {
        if (this.contentRef && this.contentRef.current) {
          // target.style.overflow = 'hidden';
          this.setState({ isOverDrawer: true });
          const contentNode = ReactDOM.findDOMNode(this.contentRef.current);
          console.log(contentNode);
          const headerNode = ReactDOM.findDOMNode(this.headerRef.current);
          // headerNode.style.backgroundColor = '#393939f9';
          // this.preventBodyScroll = true;
        }
      }
    } else if (this.preventBodyScroll) {
      this.preventBodyScroll = false;
    }
  };

  render() {
    const { isOverDrawer } = this.state;
    const { data } = this.props;
    const aboutContent = data.allMarkdownRemark.edges;
    const projectsContent = {
      ownedRepos: data.github.user.repositories.nodes,
      contributedRepos: data.github.user.repositoriesContributedTo.nodes
    };

    return (
      <Main>
        <GlobalStyle />
        <Background />
        <BackgroundOverlay />
        {/* <IntroSection headerRef={this.headerRef} /> */}
        <ContentWrapper
          flexDirection={['column', 'column', 'row']}
          justifyContent={['flex-start', 'flex-start', 'center']}
          alignItems={['center', 'center', 'normal']}
          px={[2, 4, 7]}
        >
          <ContentSplit width={[1, 1, 1 / 2]} pr={[0, 1, 3]} mt={[4, 4, 0]}>
            <Hero />
          </ContentSplit>
          <ContentSplit width={[1, 1, 1 / 2]} pl={[0, 1, 3]} mb={7}>
            <Parallax target={typeof window !== 'undefined' && window}>
              <AboutSection content={aboutContent} />
              <ProjectsSection content={projectsContent} />
            </Parallax>
          </ContentSplit>
        </ContentWrapper>
      </Main>
    );
  }
}

export const aboutPageQuery = graphql`
  query IndexQuery {
    allMarkdownRemark(
      filter: { frontmatter: { templateKey: { eq: "about-section" } } }
      sort: { order: ASC, fields: frontmatter___listOrder }
    ) {
      edges {
        node {
          html
          frontmatter {
            title
          }
        }
      }
    }
    github {
      user(login: "sltenorio99") {
        repositories(
          first: 10
          orderBy: { field: UPDATED_AT, direction: DESC }
        ) {
          nodes {
            id
            name
            description
            url
            isFork
          }
        }
        repositoriesContributedTo(first: 10) {
          nodes {
            id
            name
            description
            url
          }
        }
      }
    }
  }
`;
