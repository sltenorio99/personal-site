# My Personal Site

Bootstrapped with [Gatsby](https://www.gatsbyjs.org/) 

Uses the gatsby-starter-hello-world starter.

## Running in development
`gatsby develop`

## Building
`gatsby build`
