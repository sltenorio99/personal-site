require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`
});

exports.onCreateWebpackConfig = ({ plugins, actions }) => {
  actions.setWebpackConfig({
    plugins: [
      plugins.define({
        "process.env": {
          GITLAB_TOKEN: JSON.stringify(process.env.GITLAB_TOKEN)
        }
      })
    ]
  });
};
