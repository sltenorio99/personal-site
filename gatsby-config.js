require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`
});

module.exports = {
  siteMetadata: {
    title: `Shane-Lewes Tenorio`,
    siteUrl: `https://www.shanetenorio.com`,
    description: `Shane-Lewes Tenorio's personal website`
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/content`,
        name: "content-pages"
      }
    },
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-plugin-netlify-cms`
    },
    {
      resolve: "gatsby-source-graphql",
      options: {
        typeName: "GitHub",
        fieldName: "github",
        // Url to query from
        url: "https://api.github.com/graphql",
        // HTTP headers
        headers: {
          Authorization: `token ${process.env.GITHUB_TOKEN}`
        },
        // refetch interval in seconds
        refetchInterval: 60,
        // Additional options to pass to node-fetch
        fetchOptions: {}
      }
    },
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {
        // Add options here
      }
    },
    {
      resolve: `gatsby-plugin-typescript`,
      options: {
        // Add options here
      }
    }
  ]
};
